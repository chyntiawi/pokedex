import getConfig from 'next/config'
import Image from 'next/image'
import Link from 'next/link'
import SpeciesCardStyles from '../styles/species-card.module.css'
import { formatSpeciesId } from '../utils/text'

interface SpeciesCardProps {
  totalSpecies: number
  id: number
  name: string
}

export default function SpeciesCard({ totalSpecies, id, name }: SpeciesCardProps) {
  const { publicRuntimeConfig } = getConfig()
  const { imageUrl } = publicRuntimeConfig

  return (
    <Link href={`/pokemons/${name}`}>
      <div className={SpeciesCardStyles.card}>
        <Image
          src={`${imageUrl}/${id}.png`}
          alt="species"
          width={150}
          height={150}
        />
        <p>{ formatSpeciesId(id, totalSpecies.toString().length) }</p>
        <h2 className={SpeciesCardStyles.name}>{ name }</h2>
      </div>
    </Link>
  )
}