import SpeciesStatStyles from '../styles/species-stat.module.css'
import { PokemonStat } from '../types/pokemon'

interface SpeciesStatProps {
  pokemonStat: PokemonStat
}

export default function SpeciesStat({ pokemonStat }: SpeciesStatProps) {
  const { stat, base_stat } = pokemonStat

  return (
    <>
      <div className={SpeciesStatStyles.title}>
        <p className={SpeciesStatStyles.name}>{stat.name}</p>
        <p>{base_stat}</p>
      </div>
      <div className={SpeciesStatStyles.progress}>
        <div className={SpeciesStatStyles.progress__bar} style={{ width: `${base_stat * 2}px` }} />
      </div>
    </>
  )
}