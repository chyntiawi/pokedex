import type { NextPage } from 'next'
import getConfig from 'next/config'
import Head from 'next/head'
import { useCallback, useEffect, useRef, useState } from 'react'
import SpeciesCard from '../containers/species-card'
import HomeStyles from '../styles/home.module.css'
import { PokemonSpecies } from '../types/pokemon'

const Home: NextPage = () => {
  const { publicRuntimeConfig } = getConfig()
  const { apiUrl } = publicRuntimeConfig

  const [meta, setMeta] = useState({count: 0, next: `${apiUrl}/v2/pokemon-species?limit=100&offset=0`, previous: null})
  const [species, setSpecies] = useState([] as any[])
  const [pageNumber, setPageNumber] = useState(1)

  useEffect(() => {
    if(meta.next) {
      fetch(meta.next)
      .then((res) => res.text())
      .then((data) => {
        const { count, next, previous, results } = JSON.parse(data)

        setMeta({
          count,
          next,
          previous
        })

        setSpecies(prevSpecies => [...new Map([...prevSpecies, ...results].map(item =>
          [item.name, item])).values()])
      })
    }
  }, [pageNumber])

  const observer = useRef()
  const lastElementRef = useCallback((node: any) => {
    if(observer.current) observer.current = (observer.current as any).disconnect()

    observer.current = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting) {
        setPageNumber(prevPageNumber => prevPageNumber + 1)
      }
    }) as any

    if(node) (observer.current as any).observe(node)
  }, [meta.next])


  return (
    <>
      <Head>
        <title>Pokedex</title>
        <meta name="description" content="Pokedex" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1>Pokedex ({ meta.count })</h1>
        <div className={HomeStyles.grid}>
          {
            species.length && species.map((item: PokemonSpecies, index: number) => {
              const splittedUrl = item.url.split('/')
              const speciesId = Number(splittedUrl[splittedUrl.length - 2])

              const isLastSpecies = index + 1 === species.length
              if(isLastSpecies) {
                return <div ref={lastElementRef} key={speciesId}><SpeciesCard totalSpecies={meta.count} id={speciesId} name={item.name} /></div>
              } else {
                return <SpeciesCard key={speciesId} totalSpecies={meta.count} id={speciesId} name={item.name} />
              }
            })
          }
        </div>
      </main>
    </>
  )
}

export default Home
