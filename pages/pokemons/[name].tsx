import type { GetStaticPaths, GetStaticProps, InferGetStaticPropsType, NextPage } from 'next'
import getConfig from 'next/config'
import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'
import SpeciesStat from '../../containers/species-stat'
import PokemonsDetailStyles from '../../styles/pokemons-detail.module.css'
import { PokemonSpecies, PokemonStat } from '../../types/pokemon'
import { formatSpeciesId } from '../../utils/text'

interface PokemonType {
  type: {
    name: string
  }
}

interface PokemonDetail {
  id: number
  name: string
  types: PokemonType[]
  stats: PokemonStat[]
}

const { publicRuntimeConfig } = getConfig()
const { apiUrl, imageUrl } = publicRuntimeConfig

export const getStaticPaths: GetStaticPaths = async () => {
  const res = await fetch(`${apiUrl}/v2/pokemon`)
  const data = await res.text()
  const parsedData = JSON.parse(data)
  const paths = parsedData.results.map((pokemon: PokemonSpecies) => {
    return {
      params: {
        name: pokemon.name
      }
    }
  })

  return { paths, fallback: false }
}

export const getStaticProps: GetStaticProps = async (context: any) => {
  const name = context.params.name
  const res = await fetch(`${apiUrl}/v2/pokemon/${name}`)
  const data = await res.text()
  const pokemon = JSON.parse(data)

  return {
    props: { pokemon }
  }
}

const PokemonDetail: NextPage = ({ pokemon }: InferGetStaticPropsType<typeof getStaticProps>) => {
  const { id, name, types, stats } = pokemon as PokemonDetail

  const router = useRouter()

  return (
    <>
      <Head>
        <title>Pokedex</title>
        <meta name="description" content="Pokedex" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={PokemonsDetailStyles.container}>
        <div className={PokemonsDetailStyles.header}>
          <button className={PokemonsDetailStyles.header__button} type="button" onClick={() => router.back()}>
            <Image
              src={require('../../assets/icons/arrow.svg')}
              alt="back button"
              width={22}
              height={22}
            />
          </button>
          <h1 className={PokemonsDetailStyles.header__title}>{ `${name} (${formatSpeciesId(id)})` }</h1>
        </div>
        <div className={PokemonsDetailStyles.body}>
          <Image
            className={PokemonsDetailStyles.image}
            src={`${imageUrl}/${id}.png`}
            alt="species"
            width={250}
            height={250}
          />
          <p className={PokemonsDetailStyles.title}>Type</p>
          <div className={PokemonsDetailStyles.chip__container}>
            {
              types.map(({ type }: PokemonType, index: number) => <div key={index} className={PokemonsDetailStyles.chip}>{ type.name }</div>)
            }
          </div>
          <p className={PokemonsDetailStyles.title}>Base Stats</p>
          {
            stats.map((stat: PokemonStat, index: number) => <SpeciesStat key={index} pokemonStat={stat} />)
          }
        </div>
      </main>
    </>
  )
}

export default PokemonDetail
