export const formatSpeciesId = (id: number, totalDigits: number = 3) => {
  const idDigits = id.toString().length
  const digitsDiff = totalDigits - idDigits
  if (digitsDiff) {
    let additionalNumber = ''
    for (let i = 0; i < digitsDiff; i++) {
      additionalNumber += '0'
    }

    return `#${additionalNumber}${id}`
  }

  return `#${id}`
}