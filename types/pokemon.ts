export interface PokemonSpecies {
  name: string
  url: string
}

export interface PokemonStat {
  base_stat: number
  stat: {
    name: string
  }
}